<?php

namespace pasos\Http\Controllers;

use Illuminate\Http\Request;

use pasos\Http\Requests;

use pasos\Person;

class Basicocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $persons =Person::all();

        return  view('layout.inicio')->with("datos",$persons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("layout.parte1");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         Person::create($request->all());
          return Redirect('basicos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        return "has llegado mostrar";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $persona =   Person::findOrFail($id);
     /**      {{--return  View ("layout.Editar",["persona"=> $persona])}}

    *
    */
        return  View ("layout.Editar")->with('persona', $persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                        $datos = person::findOrFail($id);
                        $datos-> fill($request->all());
                        $datos->save();
                          return redirect('basicos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datos = Person::findOrFail($id);
        $datos->delete();
        return redirect('basicos');

    }
}
