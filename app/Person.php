<?php

namespace pasos;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
      protected $table="persons";
      protected  $primarykey="id";

      protected $fillable=["name","last_name"];



}
