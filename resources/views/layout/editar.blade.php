@extends('layout.template')

@section('title','titulo')


@section('contenido')

<div class="container">
            <h1>EDITAR DATOS</h1></br>


{!! Form::model($persona, ['route' => ['basicos.update', $persona->id], 'method' => 'PUT' , "class"=>"form-horizontal"]) !!}

<div class="form-group ">
  {!! Form::label("name","Nombre",[" class"=>"control-label col-md-3"])!!}
  <div class="col-md-6">
  {!!Form::text("name",null,["class"=>"form-control ", "placeholder"=>"Ingrese Primer Nombe "])!!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('Last_name',"Segundo Nombre",["class"=>"control-label col-md-3"]) !!}
  <div class="col-md-6">
      {!!Form::text('last_name',null,["class"=>"form-control",'placeholder'=>'ingrese Segundo Nombre'])!!}
  </div>

</div>

{!! Form::submit('Editar', ['class' => 'btn btn-primary  ']) !!}

{!! Form::close() !!}


</div>
<!-- ciere de div-->
<!--
 * <form class="form-horizontal" action="index.html" method="post">
 */

    <div class="form-group col-md-offset-4">
          <label for="Name" class="col-md-1 col-md-offset-3">Nombre:</label>
          <div class="col-md-4">
            <input type="text" class="form-control " id="Name" placeholder="ingrese Nombre">

          </div>

  </div>
  <div class="form-group">
        <label for="" class="col-md-1 col-md-offset-3">Segundo Nombre</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="" placeholder="Segundo Nombre">
        </div>


</div>
    <button type="submit" name="button" class=" btn btn-primary">Ingresar Datos</button>
</form>-->




@endsection
